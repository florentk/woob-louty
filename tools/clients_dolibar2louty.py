#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Convertir un export de clients Dolibar vers un export compatible avec l'importation de client dans Woob Business 

usage : clients_dolibar2louty.py [-h | --help] [-o | --output] [-l | --list-rate] fichier1, fichier2, ...
  -o : Fichier couverti
  -s : Découpe l'export en deux fichier : une pour les particulier et une pour les professionels
  -h : Affiche cette aide

"""

import sys
import csv
import codecs
import getopt

from process_client import process_client

WOOB_HEADER = ["nom","prenom","adresse number","adresse","adresse complement","code postal","ville","tel","email","cat","memo"]

def writeWoobCSVFile(csv_file, rows):
    with codecs.open(csv_file, 'w', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(WOOB_HEADER)
        writer.writerows(rows)

def dolibar2woobBusiness(csv_file, enc):
    woob_lines = []
    with codecs.open(csv_file, 'r', encoding=enc) as f:
        reader = csv.reader(f)

        #read the header
        first = next(reader)

        #Dolibar fields
        name_index = first.index(u'Nom')
        addr_index = first.index(u'Adresse')
        cp_index = first.index(u'Code postal')
        city_index = first.index(u'Ville')
        tel_index = first.index(u'Téléphone')
        email_index = first.index(u'Email')
        cat_index = first.index(u'Type du tiers')
        code_index = first.index(u'Code client')

        for row in reader:
            name = row[name_index].strip()
            addr = row[addr_index].strip()
            cp = row[cp_index].strip()
            city = row[city_index].strip()
            tel = row[tel_index].strip()
            email = row[email_index].strip()
            cat = row[cat_index].strip()
            code = row[code_index].strip()

            woob_lines.append(list(process_client(name,addr,cp,city,tel,email,cat,code)))
    return woob_lines


def main():
    output_file = "woob_business_clients.csv"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hso:", ["help","split","output"])
    except getopt.GetoptError as e:
        print ("erreur : %s" % str(e.msg))
        print ("pour afficher l'aide utiliser --help")
        sys.exit(2)

    split = False

    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        if o in ("-o", "--output"):
            output_file = a
        if o in ("-s", "--split"):
            split = True

    print("Exporte vers le fichier %s" % output_file)

    for arg in args:
        print ("Convertion de %s" % arg)
        rows = dolibar2woobBusiness(arg, "Latin-1")
        if (split):
            writeWoobCSVFile("%s-%s" % (output_file,"particuliers"), filter(lambda r: r[9] == "individual",rows))
            writeWoobCSVFile("%s-%s" % (output_file,"professionels"), filter(lambda r: r[9] == "professionnal",rows))
        else:
            writeWoobCSVFile(output_file, rows)

if __name__ == "__main__":
    main()
