#!/usr/bin/env python3
# -*- coding: utf-8 -*-

ADDRLIMIT=60
NOMLIMIT=50
PRENOMLIMIT=30

def split_name_individual(name):
    firstname=" ".join(filter( lambda s: not s.isupper(), name.split()))
    name=" ".join(filter( lambda s: s.isupper(), name.split()))

    #if no name, consider fisrtname as the name
    if(name==""):
        name = firstname
        firstname = ""

    return (firstname, name)

def clean_tel(tel):
    if(tel):
        if(tel.isdigit()):
            tel = "0%s" % tel
        tel = tel.replace(".","")
        tel = tel.split('\\n')[0]
    return tel

def convert_categroy(cat):
    if(cat=='Particulier'):
        return "individual"
    return "professionnal"

def split_address(addr):
    addr = addr.replace(","," ")
    addrlines = addr.split('\\n')
    extra = " ".join(addrlines[1:]) if(len(addrlines)>1) else ''
    addr = addrlines[0]
    addrs = addr.split()
    num = addrs[0]
    if(num.isdigit()):
        return (num, " ".join(addrs[1:]), extra)
    return "", addr, extra

def process_client(name,addr,cp,city,tel,email,cat,code):
    cat = convert_categroy(cat)
    firstname,name = split_name_individual(name) if (cat=="individual") else ("",name)
    addr_num, addr, addr_ex = split_address(addr) if addr else ("","N/A","")
    return name[:NOMLIMIT],firstname[:PRENOMLIMIT],addr_num, addr[:ADDRLIMIT], addr_ex[:ADDRLIMIT],cp,city,clean_tel(tel),email,cat,code

