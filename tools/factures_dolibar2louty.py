#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Convertir un export de factures Dolibar vers un export compatible avec l'importation de client dans Woob Business 

Les fichier doivent être au format CSV avec une virgule comme séparation de champs

usage : factures_dolibar2louty.py [-h | --help] [-o | --output] [-c | --code_article] [-u | --unit] [-l : --libele_produit] [-a : --code_activite] [-t : --income_by_unit] [-r --reglement_export] [-d --paiement_due_config] factures_export_csv
  -o : Fichier converti
  -u : Unité pour la ligne de facturation dans Louty : h (Heure), m (Minute), j (Jour) ...
  -c : Code article dans Louty (ex: ENTRETIEN MAISON)
  -l : Libellé produit
  -a : Code activité
  -t : Rémuneration par unité (defined by -u) including tax
  -r : Fichier d'export dolibar contenant le mode de réglement des factures
  -d : Fichier de configuration des écheances de paiements par défaut selon le mode
  -h : Affiche cette aide


"""

import sys
import csv
import codecs
import getopt
import re

from datetime import datetime
from bs4 import BeautifulSoup
from process_client import process_client
from configparser import ConfigParser
from unidecode import unidecode

DESC_LIMIT=40
WOOB_HEADER = ["Account","Title","Date","Payment choice","Payment due ID","Code article","Description","Price Unit","Including tax","Tax rate","Unit ID","Quantity","Amount","Amount VAT","Activity ID","Memo","First Name","Last Name","Address Number","Address","Address ex","Zip code","City","Tel", "Customer Category","Customer ID"]

DEFAULT_PAYEMENT_CHOICE = "Virement"
DEFAULT_PAYEMENT_DUE = {'Virement': "5 jours", "Cheque": "Acquittée"}

def format_date(date):
    return datetime.strptime(date, '%Y-%m-%d').strftime("%d/%m/%Y")

def clean_memo(memo):
    soup = BeautifulSoup(memo,"lxml")
    return soup.get_text()

def check_account_name(name):
    firstname=" ".join(filter( lambda s: not s.isupper(), name.split()))
    name=" ".join(filter( lambda s: s.isupper(), name.split()))

    #if no name, consider fisrtname as the name
    if(name==""):
        name = firstname
        firstname = ""

    if(firstname==""):
        firstname = "N/A"

    return "%s %s" % (firstname, name)

def recompute_amounts(income_by_unit_tax_inc, tax_rate, amount_vat, price_unit,quantity,amount):
    price_unit = income_by_unit_tax_inc
    quantity = float(amount_vat.replace(',', '.')) / price_unit
    amount =  float(amount_vat.replace(',', '.')) / (1.0 + float(tax_rate.replace(',', '.')) / 100.0)
    includingTax = 1
    return str(price_unit), str(quantity), str(amount), includingTax

def read_reglement_export(csv_file, enc):
    paiements = {}

    if(not csv_file):
        return paiements

    with codecs.open(csv_file, 'r', encoding=enc) as f:
        reader = csv.reader(f,delimiter=',', quotechar='"')

        first = next(reader)

        try:
          ref_index = first.index(u'Réf. facture')
          paiement_index = first.index(u'Type de paiement (libellé)')

        except ValueError as e:
          print("Format de fichier incorect : %s" % e)
          print("En-tête : %s" % first)
          return paiements

        for row in reader:
            paiement = row[paiement_index].strip()
            ref = row[ref_index].strip()
            paiements[ref] = paiement
        
    return paiements

def read_paiement_due_config(paiement_due_config):
    if (not paiement_due_config):
        return DEFAULT_PAYEMENT_DUE, DEFAULT_PAYEMENT_CHOICE

    config_object = ConfigParser()
    config_object.read(paiement_due_config)
    paiement_due = dict(config_object["DEFAULT"].items())
    paiement_choice = paiement_due.pop("default")

    return paiement_due, paiement_choice

def dolibar2woobBusiness_line(account,title,date,paiement,paiement_due_id,code_article,description,price_unit,tax_rate,unit_id,quantity,amount,amount_vat,activity_id,memo,addr, cp, city, tel, code_client, income_by_unit_tax_inc, category):
    includingTax = 0

    #remove multiple space
    account=re.sub(' +', ' ', account)

    if(income_by_unit_tax_inc):
        price_unit, quantity, amount, includingTax = recompute_amounts(income_by_unit_tax_inc, tax_rate, amount_vat, price_unit, quantity, amount)

    first_name,last_name,addr_num, addr, addr_ex, cp, city, tel, email, category, code_client = process_client(account,addr,cp,city,tel,"",category,code_client)

    return   (check_account_name(account) if category=="individual" else account,
              title,
              format_date(date),
              paiement,
              paiement_due_id,
              code_article,
              description[:DESC_LIMIT],
              price_unit,
              includingTax,
              tax_rate,
              unit_id,
              quantity,
              amount,
              amount_vat,
              activity_id,
              clean_memo(memo),
              first_name,
              last_name,
              addr_num,
              addr,
              addr_ex,
              cp,
              city,
              tel,
              category,
              code_client)

def writeWoobCSVFile(csv_file, rows):
    with codecs.open(csv_file, 'w', encoding='utf-8') as f:
        writer = csv.writer(f, delimiter=';', quotechar='"')
        writer.writerow(WOOB_HEADER)
        writer.writerows(rows)

def dolibar2woobBusiness(csv_file, code_article, unit, code_activite, fixed_libele_produit, income_by_unit_tax_inc, reglement_export, paiement_due_config, category, enc):
    paiements = read_reglement_export(reglement_export, enc)
    default_payement_due, default_payement_choice = read_paiement_due_config(paiement_due_config)

    woob_lines = []
    with codecs.open(csv_file, 'r', encoding=enc) as f:
        reader = csv.reader(f,delimiter=',', quotechar='"')

        #read the header
        first = next(reader)

        #check if file is already convert
        if(first[0]=='Account;Title;Date;Payment choice;Payment due ID;Code article;Description;Price Unit;Including tax;Tax rate;Unit ID;Quantity;Amount;Amount VAT;Activity ID;Memo;erreur'):
            return sys.exit(9)

        try:
          #Dolibar fields
          ref_index = first.index(u'Réf. facture')
          date_index = first.index(u'Date facturation')
          prix_unit_index = first.index(u'Prix unitaire de la ligne')
          quantite_index = first.index(u'Quantité pour la ligne')
          tax_rate_index = first.index(u'Taux de TVA de la ligne')
          description_index = first.index(u'Description de ligne')
          montant_ht_index = first.index(u'Montant HT de la ligne')
          montant_ttc_index = first.index(u'Montant TTC de la ligne')
          ref_produit_index = first.index(u'Réf. produit')
          libele_produit_index = first.index(u'Libellé produit')

          name_index = first.index(u'Raison sociale')
          addr_index = first.index(u'Adresse')
          cp_index = first.index(u'Code postal')
          city_index = first.index(u'Ville')
          tel_index = first.index(u'Téléphone')
          code_index = first.index(u'Code client')

        except ValueError as e:
          print("Format de fichier incorect : %s" % e)
          print("En-tête : %s" % first)
          return woob_lines

        for row in reader:
            try:
              ref = row[ref_index].strip()
              date = row[date_index].strip()
              prix_unit = row[prix_unit_index].strip()
              tax_rate = row[tax_rate_index].strip()
              quantite = row[quantite_index].strip()
              description = row[description_index].strip()
              montant_ht = row[montant_ht_index].strip()
              montant_ttc = row[montant_ttc_index].strip()
              ref_produit = row[ref_produit_index].strip()

              name = row[name_index].strip()
              addr = row[addr_index].strip()
              cp = row[cp_index].strip()
              city = row[city_index].strip()
              tel = row[tel_index].strip()
              code_client = row[code_index].strip()

            except IndexError as e:
              print("Format de fichier incorect : %s" % e)
              print("Ligne : %s" % row)
              return woob_lines

            if(fixed_libele_produit):
                libele_produit = fixed_libele_produit
            else:
                libele_produit = row[libele_produit_index].strip()

            if(ref in paiements):
                paiement = unidecode(paiements[ref]).lower()
            else:
                paiement = default_payement_choice

            if(paiement in default_payement_due):
                paiement_due_id = default_payement_due[paiement]
            else:
                paiement = default_payement_choice
                paiement_due_id = default_payement_due[default_payement_choice]

            woob_lines.append(list(
                                dolibar2woobBusiness_line(
                                    name,
                                    ref,
                                    date,
                                    paiement,
                                    paiement_due_id,
                                    code_article,
                                    libele_produit,
                                    prix_unit,
                                    tax_rate,
                                    unit,
                                    quantite,
                                    montant_ht,
                                    montant_ttc,
                                    code_activite,
                                    description,
                                    addr,
                                    cp,
                                    city,
                                    tel,
                                    code_client,
                                    income_by_unit_tax_inc,
                                    category
                                  )
                             )
           )
    return woob_lines


def main():
    output_file = "woob_business_factures.csv"
    code_article = None
    libele_produit = None
    unit = None
    code_activite = ''
    income_by_unit_tax_inc = None
    reglement_export = None
    particulier = False
    paiement_due_config = None

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:c:u:l:a:t:r:d:p", ["help","output=","code_article=","unit=","libele_produit=","code_activite=","income_by_unit=","reglement_export=","paiement_due_config=","particulier"])
    except getopt.GetoptError as e:
        print ("erreur : %s" % str(e.msg))
        print ("pour afficher l'aide utiliser --help")
        sys.exit(2)

    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        if o in ("-o", "--output"):
            output_file = a
        if o in ("-c", "--code_article"):
            code_article = a
        if o in ("-u", "--unit"):
            unit = a
        if o in ("-l", "--libele_produit"):
            libele_produit = a
        if o in ("-a", "--code_activite"):
            code_activite = a
        if o in ("-t", "--income_by_unit"):
            income_by_unit_tax_inc = float(a.replace(',', '.'))
        if o in ("-r", "--reglement_export"):
            reglement_export = a
        if o in ("-d","--paiement_due_config"):
            paiement_due_config = a
        if o in ("-p", "--particulier"):
            particulier = True

    if(not (code_article or unit)):
        print ("code article (-c code) et unité (-u unité) nécessaire")
        print ("pour afficher l'aide utiliser --help")
        sys.exit(3)

    #print("Export vers le fichier %s" % output_file)

    for arg in args:
        #print ("Convertion de %s" % arg)
        rows = dolibar2woobBusiness(arg, code_article, unit, code_activite, libele_produit, income_by_unit_tax_inc, reglement_export, paiement_due_config, particulier, "Latin-1")
        writeWoobCSVFile(output_file, rows)

if __name__ == "__main__":
    main()
