FROM debian:buster-slim
#
# Author : Florent Kaisser <florent.pro@kaisser.name>
#
LABEL maintainer="opteos"
ENV WOOB_BIN="/root/woob"
ENV PATH="/root/.local/bin:${PATH}"
ENV WOOB_CONFIG_PATH="/root/.config/woob"
ENV WOOB_BACKENDS_FILE="$WOOB_CONFIG_PATH/backends"

RUN apt-get update && \
    apt-get install -y --no-install-recommends git vim python3 python3-pip python3-setuptools  libjpeg-dev zlib1g-dev  build-essential libssl-dev libffi-dev python3-dev python3-bs4  && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*

RUN git clone -c http.sslverify=false --depth=1 --branch=master https://framagit.org/florentk/woob.git && \
    cd woob && \
     pip3 install wheel awscli && \ 
     ./tools/local_install.sh $WOOB_BIN && \
     rm -rf woob

WORKDIR /root

RUN git clone -c http.sslverify=false --depth=1 https://framagit.org/florentk/woob-louty.git

RUN woob update && \
    echo "file:///$PWD/woob-louty" >> "$WOOB_CONFIG_PATH/sources.list" && \
    woob update && \
    rm $WOOB_BACKENDS_FILE
    
COPY backends $WOOB_BACKENDS_FILE
RUN chmod 400 $WOOB_BACKENDS_FILE

CMD [ "woob", "business"]
