#!/bin/bash

WOOB_BIN=~/.local/bin/woob
WOOB_LOUTY=~/Projets/woob-louty

cd /tmp

git clone -c http.sslverify=false --depth=1 --branch=master https://framagit.org/florentk/woob.git && \
    cd woob && \
     ./tools/local_install.sh "$WOOB_BIN" 

cd "$WOOB_LOUTY"

git pull
woob update

sleep 3
