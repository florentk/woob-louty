#!/bin/bash

ACTION=$1
DIR="$(dirname $0)"

. $DIR/config_structure

PDF_DIR=$DIR/pdf

EXPORT_PATH=$DIR"/export dolibarr"
REGLEMENT_FILE="$EXPORT_PATH/reglements.csv"
REMUNERATION_PAR_UNIT=1
PAYEMENTS_ALTERNA="$DIR/payements_alterna.ini"

error () {
    echo "Appuyer sur une touche pour quitter ..."
    read -n 1 ch
    exit 1
}

if [ ! -f "$REGLEMENT_FILE" ]; then
    echo "Vous devez copier le fichier reglements.csv dans $EXPORT_PATH"
    error
fi

if ! command -v  woob > /dev/null 2>&1 ; then 
    echo "woob n'est pas installé"
    error
fi

# import2louty file louty_structure_id options
import2louty () {
    file=$1
    struct=$2
    code_article=$3
    label=$4
    unite=$5
    opt=$6
    output="$file.louty"

    cd $DIR/log

    "$DIR"/../../tools/factures_dolibar2louty.py -c "$code_article" -u "$unite" -l "$label" -o "$output" -r "$REGLEMENT_FILE" $opt "$file"

    #file already convert, just copy
    if [ $? -eq 9 ];then
       cp "$file" "$output"
    fi

    woob business change_structure $struct 
    woob business import_document "$output" $ACTION "$PDF_DIR"

    rm -f "$output"
    
    cd ..
}

for file in "$EXPORT_PATH"/*.csv
do
    if [ "$file" = "$EXPORT_PATH/reglements.csv" ]; then
        continue
    fi

    TRAITE=1

    $DIR/filtre_structure.py "$file" "$DIR/alterna_export.csv" "$DIR/ge_export.csv"

    mv "$file" "$DIR/archives/"

    echo "Traite ALTERNA"
    import2louty "$DIR/alterna_export.csv" $STRUCT_ID_ALTERNA "$CODE_ARTICLE_ALTERNA" "$LABEL_ALTERNA" "$UNITE_ALTERNA" "-t $REMUNERATION_PAR_UNIT -d $PAYEMENTS_ALTERNA -p"

    echo "Appuyer sur une touche pour continuer ..."
    read -n 1 ch
    rm "$DIR/alterna_export.csv" 

    echo "Traite GRAND ENSEMBLE"
    import2louty "$DIR/ge_export.csv" $STRUCT_ID_GE "$CODE_ARTICLE_GE" "$LABEL_GE" "$UNITE_GE"

    echo "Appuyer sur une touche pour continuer ..."
    read -n 1 ch
    rm "$DIR/ge_export.csv"
done

mv "$REGLEMENT_FILE" "$DIR/archives/"

if [ -z $TRAITE ] ; then
    echo "Aucun fichier a traiter"
else
    echo "Fin de l'export"
fi
sleep 3

