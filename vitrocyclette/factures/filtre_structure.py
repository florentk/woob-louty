#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import csv
import codecs

COL_TVA=39
TVA_ALTERNA="10.000"

csv_file = sys.argv[1]
alterna_file = sys.argv[2]
ge_file = sys.argv[3]

with codecs.open(csv_file, 'r', encoding= "Latin-1") as f:
    rows_alterna = []
    rows_ge = []

    reader = csv.reader(f,delimiter=',', quotechar='"')

    first = next(reader)

    rows_alterna.append(first)
    rows_ge.append(first)

    for row in reader:
        if row[COL_TVA] == TVA_ALTERNA:
            rows_alterna.append(row)
        if row[COL_TVA] != TVA_ALTERNA:
            rows_ge.append(row)

    with codecs.open(alterna_file, 'w', encoding="Latin-1") as f:
        writer = csv.writer(f, delimiter=',', quotechar='"')
        writer.writerows(rows_alterna)

    with codecs.open(ge_file, 'w', encoding="Latin-1") as f:
        writer = csv.writer(f, delimiter=',', quotechar='"')
        writer.writerows(rows_ge)
