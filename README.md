# Manual install without Docker

You need to install a fork of Woob for Business application :

```
git clone https://framagit.org/florentk/woob
```

Then, in the woob repository, install Woob as Developper :

```
mkdir -p ~/bin
cd woob
./tools/local_install.sh ~/bin/woob
```

Run as first time a update to init config file :

```
woob update
```

Add the local path of woob-louty repository at the end of `~/.config/woob/sources.list`. Ex: 

```
echo "file:///home/me/src/woob-louty" >> ~/.config/woob/sources.list
```

Then, run again the update:

```
woob update
```

Finally, we can run business application :

```
woob business
```

And choose Louty as backend. 

# Import expense

On prompt of Woob:

```
business> import_expense expences.csv
```

To validate the expenses immediately after import : 

```
business> import_expense expences.csv valid
```

We need to consider this CSV format :

```
"Matricule";"Utilisateur";"Date de la dépense";"Commentaire";"Fournisseur";"Nature";"Activité à imputer";"Montant HT";"TVA récupérée";
"AACOVER0";"Harry Cover";"09/10/2021";"Facture internet";"Bouygues telecom";"Frais téléphone et internet";"";"16,49";"0";
"AACOVER0";"Harry Cover";"11/10/2021";"Repas du midi";"Aux enfants";"Repas lors deplacement";"";"9,07";"0,5";
"AAPAUL0";"Pierre Paul";"09/10/2021";"Materiel";"LDLC";"Ordinateur portable";"";"499,167";"99,833";
```

# Import customers

On prompt of Woob:

```
business> import_client clients.csv
```

We need to consider this CSV format :

```
nom;prenom;adresse number;adresse;adresse complement;code postal;ville;tel;email;cat;memo
Harry;Cover;50;avenue de la république;batiment B;59000;LILLE;0647851478;harry.cover@domain.com;individual;code : 2345
ACME;;;chemin de l'arbre;;45770;Saran;0345715844;;professional;
```

# Using Docker

edit `backends` file to set Louty login

```
docker build -t woob-louty  ./

docker run -it woob-louty
```

