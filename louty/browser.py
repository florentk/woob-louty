# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
import json

from woob.browser.retry import login_method, RetryLoginBrowser
from woob.browser.browsers import need_login
from woob.browser.url import URL
from woob.exceptions import BrowserIncorrectPassword
from .pages import LoginPage, DocsPage, PreDocsPage, AccueilPage, ClientsPage, PreClientsPage, PiecePage, PieceEditPage, UpdatePage, NotesFraisPage, NoteFraisPage, NoteFraisEditPage, ErrorPage
from woob.browser.exceptions import ClientError, ServerError
from woob.tools.compat import basestring
from woob.capabilities.customer import Customer

__all__ = ['LoutyBrowser']

CUSTOMER_CATEGORIES = {'professionnal':1, 'individual':2, 'community':3}

class LoutyBrowser(RetryLoginBrowser):
    BASEURL = 'https://appli.louty.net/'

    loginpage = URL('/Louty/login', LoginPage)

    #predocspage = URL('/GestionCom/ExportTableurPiece/ExportTableur', PreDocsPage)
    #docspage = URL('/GestionCom/ExportTableurPiece/ExportCsv\?structureCouranteId=(?P<structureCouranteId>.*)&individuCourantId=(?P<individuCourantId>.*)', DocsPage)

    piecepage = URL ('/GestionCom/PiecesEdit/Open/(?P<pieceId>.*)', PiecePage)
    pieceeditpage = URL ('/GestionCom/PiecesEdit/Edit/(?P<pieceId>.*)', PieceEditPage)
    #https://appli.louty.net/GestionCom/PiecesEdit/Create?pieceType=FC
    piececreatepage = URL ('/GestionCom/PiecesEdit/Create/-1?pieceType=FC', PieceEditPage)
    validlinepage = URL('/GestionCom/PiecesEdit/ValideLigne', UpdatePage)

    fraiseditpage = URL('/AchatsFrais/NoteFraisEdit/Edit\?noteFraisId=(?P<noteFraisId>.*)',NoteFraisEditPage)
    fraiscreatepage = URL('/AchatsFrais/NoteFraisEdit/Create',NoteFraisEditPage)
    fraispage = URL('/AchatsFrais/NoteFrais/Index', NotesFraisPage)

    preclientspage = URL('/GestionCom/ExportTableurClient/ExportTableur\?actId=&filtreSansCompte=false&filter=&filtreAutoriseToutesActivites=true&categorieOrganisation=-1', PreClientsPage)
    clientspage = URL('/GestionCom/ExportTableurClient/ExportCsv', ClientsPage)

    docspage = URL ('GestionCom/Pieces', 'GestionCom/Pieces\?.*', DocsPage)
    recherchepage = URL ('GestionCom/Pieces', 'GestionCom/Pieces/Index\?piecesArchivees=false&recherche=(?P<recherche>.*)', DocsPage) 
    #clientcreatepage = URL('/Louty/client/create', ClientEditPage)

    errorpage = URL('/GestionCom/Erreur/Erreur500', ErrorPage)

    accueilpage = URL('/Accueil', AccueilPage)

    @login_method
    def do_login(self):
        assert isinstance(self.username, basestring)
        assert isinstance(self.password, basestring)

        try:
            self.loginpage.stay_or_go().login(self.username, self.password)
#            self.structureCouranteId = self.page.structureCouranteId
#            self.individuCourantId = self.page.individuCourantId
        except ClientError as error:
            if error.response.status_code == 401:
                raise BrowserIncorrectPassword()
            raise

    def get_nb_remaining_free_sms(self):
        raise NotImplementedError()

    def post_message(self, message, sender):
        raise NotImplementedError()
    
    @need_login
    def valide_ligne(self):
        r = self.page.valide_ligne(self.ligne)
        return self.ligne

    def return_piece_id(self,r):
        if(r['success']):
            return r['pieceId']
        else:
            raise Exception(r['message'])

    @need_login
    def enregistre_piece(self):
        r = self.page.submit("VerifiePieceCoherente")
        #print (r)
        r = self.page.submit("VerifiePieceValidee")
        #print (r)
        r = self.page.submit("Enregistre")
        #print (r)
        return self.return_piece_id(r)

    @need_login
    def transmet_piece(self):
        r = self.page.transmit()
        return self.return_piece_id(r)

    @need_login
    def valide_piece(self):
        r = self.page.valide()
        return self.return_piece_id(r)

    @need_login
    def create_ligne(self, articleCode):
        self.ligne = self.page.create_ligne(articleCode)
        return self.ligne

    @need_login
    def create_piece(self, accountId):
        self.piececreatepage.go()
        self.page.set_client(accountId)

    def set_piece_date(self, date):
        self.page.set_date(date)

    def set_piece_titre(self, titre):
        self.page.set_titre(titre)

    def set_piece_reglement_mode(self,paiement_mode):
        self.page.set_reglement_mode(paiement_mode)

    def set_piece_reglement_echeance(self,echeance):
        self.page.set_reglement_echeance(echeance)

    @need_login
    def edit_ligne(self, _id):

        self.ligne = None

        for l in self.lines:
            if (l.id == _id):
                self.ligne = l

        return self.ligne

    @need_login
    def edit_piece(self, _id):
        self.lines = []

        self.pieceId=_id
        self.pieceeditpage.go(pieceId=self.pieceId)

        for l in self.page.iter_lignes():
            self.lines.append(l)

        return iter(self.lines)

    @need_login
    def download_PDF(self, pieceId):
    
        params = {'cgvIds':'',
          'joindreDevis':'false',
          'cgvVerso':'false',
          'rectoVerso':'false',
          'pieceId':pieceId,
          'regenerer':'false',
          'fichiersJoints':'false'
        }
        
        return self.open( self.absurl( "%s/GestionCom/Pieces/ImprimePiece" % self.BASEURL), 
          params = params
        ).content

    @need_login
    def iter_lignes(self, _id):
        lines = []
        
        self.pieceId=_id
        self.piecepage.go(pieceId=_id)
        
        for l in self.page.iter_lignes():
            lines.append(l)
            
        return iter(lines)

    def result_document_list(self):
        documents = []

        for b in self.page.iter_documents():
            documents.append(b)
            
        return iter(documents)

    @need_login
    def iter_documents(self):
        self.docspage.go()
        return self.result_document_list()

    @need_login
    def recherche_documents(self, recherche):
        self.recherchepage.go(recherche=recherche)
        return self.result_document_list()

    @need_login
    def iter_clients(self):
        clients = []

        self.preclientspage.go(method='POST')
        self.clientspage.go()

        for c in self.page.iter_clients():
            clients.append(c)

        return iter(clients)

    @need_login
    def iter_frais(self):
        frais = []

        self.fraispage.go()

        for f in self.page.iter_documents():
            frais.append(f)

        return iter(frais)

    @need_login
    def iter_structure(self):
        structures = self.open( self.absurl( "%sLouty/api/Structure/getStructureUtilisateurList" % self.BASEURL)).json()
        return structures

    @need_login
    def change_structure(self, _id):
        ret = self.open( self.absurl( "%sLouty/api/Authentication/changeStructure" % self.BASEURL), data={"structureId":str(_id)}).json()
        return ret['structureNom']

    @need_login
    def iter_frais_lignes(self, _id):
        self.frais_lignes = []

        self.noteFraisId=_id
        self.fraiseditpage.go(noteFraisId=_id)

        for l in self.page.iter_lignes():
            self.frais_lignes.append(l)

        self.hasFraisListLigne = True

        return iter(self.frais_lignes)

    @need_login
    def create_frais(self, titre):
        self.fraiscreatepage.go()
        #print(self.page.individuId, self.page.activiteId)
        self.page.titre = titre
        self.hasFraisListLigne = False

    @need_login
    def set_frais_individu(self, individu):
        self.page.individuId = self.page.recherche_individu_id(individu)
        self.page.activiteId = self.page.get_activite_unique(self.page.individuId)
        #print(self.page.individuId, self.page.activiteId)

    @need_login
    def set_frais_activite(self, activite):
        self.page.activiteId = self.page.get_activite_unique(self.page.individuId)
        #print(self.page.individuId, self.page.activiteId)

    @need_login
    def submit_frais(self):
        r = self.page.submit()
        return r


    @need_login
    def transmet_frais(self):
        r = self.page.transmet_imprime()
        return r

    @need_login
    def valid_frais(self):
        r = self.page.valid()
        return r

    @need_login
    def valide_frais_ligne(self):
        self.page.valide_ligne(self.frais_ligne)
        return self.frais_ligne

    @need_login
    def set_frais_ligne_activite_a_imputer(self, activite):
        self.frais_ligne.activity_id = self.page.recherche_activite_id(activite)

    @need_login
    def create_frais_ligne(self, _id):
        self.frais_ligne = self.page.create_ligne(int(_id) * -1)
        return self.frais_ligne
        #return self.valide_frais_ligne()

    @need_login
    def edit_frais_ligne(self, _id):
        self.frais_ligne = None

        if(self.hasFraisListLigne):
          for l in self.frais_lignes:
              print (l.id, _id)
              if (l.id == _id):
                  self.frais_ligne = l

        if(not self.frais_ligne):
          self.create_frais_ligne(-1)

        return self.frais_ligne

    @need_login
    def init_create_client(self, _id):
        self.fraiscreatepage.go()
        self.activiteId = self.page.get_activite_unique(self.page.individuId)

    def create_client(self, _id):
        self.client = Customer()
        self.client.id = _id
        self.client.name = "IMPORT %s" % uuid.uuid4().hex
        self.client.firstname = "N/A"
        self.client.city = "N/A"
        self.client.postal_code = "0"
        self.client.street_name = "N/A"
        self.client.category = "professionnal"

        return self.client

    @need_login
    def save_client(self, force):
        if (self.client.category not in CUSTOMER_CATEGORIES):
            self.client.category = "professionnal"

        client_dict = {
          "keys": ["DOUBLON_NOM_PRENOM_CODEPOSTAL"] if (force) else [] ,
          "value":{
              "nom": self.client.name,
              "prenom":self.client.firstname,
              "categorieId":CUSTOMER_CATEGORIES[self.client.category],
              "memo":self.client.memo,
              "enSommeil":False,
              "activiteAutorise":{"activites":[{"id" : self.activiteId, "ok" : True}],"touteActivite":False},
              "adresses":[{
                "codePostal":self.client.postal_code,
                "commune":{"nom":self.client.city,"code":self.client.postal_code},
                "pays":None,
                "numero":self.client.street_number,
                "adresse1":None,
                "adresse2":None,
                "adresse3":self.client.street_name,
                "adresse4":self.client.street_extra,
                "typAdresseId":1
               }],
              "telephones":[{"id":None,"numeroTel":self.client.telephone,"telPrincipal":True,"memoTel":"Principal"}] if self.client.telephone else [],
              "courriels":[{"id":None,"adresseMel":self.client.email,"melPrincipal":True, "memoMel":"Principal"}] if self.client.email else [],
              "ibans":[],
              "libNaf":None,
              "codeNaf":None,
              "clientIbans":None,
              "numeroTva":None,
              "numeroSiret":None,
              "echeanceReglement":None,
              "modeReglement":None,
              "numeroNaf":None,
              "contacts":None,
              "duplication":False
            }
        };
        #print(json.dumps(client_dict))
        #self.location(self.absurl('%sLouty/client/list' % self.BASEURL))
        ret =  self.open( self.absurl( "%sLouty/api/Client/save" % self.BASEURL), data = json.dumps(client_dict), headers={'Content-Type': 'application/json'}).json()
        if('id' in ret):
            self.client.id = ret['id']
            return self.client
        else:
            raise Exception(ret['msg'])
