# -*- coding: utf-8

from woob.capabilities.commercial import CapDocument, Document, DocumentNotFound, CapLine, Line
from woob.capabilities.customer import CapCustomer, Customer, CustomerNotFound
from woob.capabilities.expense import Expense, CapExpense, Line as ExpenseLine, CapLine as CapExpenseLine, ExpenseNotFound

from .browser import LoutyBrowser

from woob.tools.backend import Module, BackendConfig
from woob.tools.value import ValueBackendPassword, Value



__all__ = ['LoutyModule']




class LoutyModule(Module, CapDocument, CapCustomer, CapLine, CapExpense, CapExpenseLine):
    NAME = 'louty'
    MAINTAINER = u'Florent Kaisser'
    EMAIL = 'florent.pro@kaisser.name'
    VERSION = '3.1'
    DESCRIPTION = 'Management of cooperativ'
    LICENSE = 'AGPLv3+'
    CONFIG = BackendConfig(Value('login', label='Login'),
                           ValueBackendPassword('password', label='Password'),
                           )
    BROWSER = LoutyBrowser


    def __init__(self, *args, **kwargs):
        self._browsers = dict()
        super(LoutyModule, self).__init__(*args, **kwargs)

    def create_default_browser(self):
        return self.create_browser(self.config['login'].get(), self.config['password'].get())

    def get_document(self, _id):
        subid = _id.rsplit('_', 1)[0]
        subscription = self.get_subscription(subid)
        return find_object(self.iter_documents(), id=_id, error=DocumentNotFound)

    def iter_documents(self):
        return self.browser.iter_documents()

    def search_documents(self, search):
        return self.browser.recherche_documents(search)

    def iter_customers(self):
        return self.browser.iter_clients()

    def iter_lines(self, _id):
        return self.browser.iter_lignes(_id)

    def iter_structure(self):
        return self.browser.iter_structure()

    def edit_document(self, _id):
        return self.browser.edit_piece(_id)

    def submit_document(self):
        return self.browser.enregistre_piece()

    def transmit_document(self):
        return self.browser.transmet_piece()

    def valid_document(self):
        return self.browser.valide_piece()

    def edit_line(self, _id):
        return self.browser.edit_ligne(_id)
        
    def valid_line(self, _id):
        return self.browser.valide_ligne()
    
    def download_PDF(self, pieceId):
        return self.browser.download_PDF(pieceId)
    
    def create_line(self, articleCode):
        return self.browser.create_ligne(articleCode)
        
    def create_document(self, accountId):
        return self.browser.create_piece(accountId)

    def set_document_title(self, title):
        return self.browser.set_piece_titre(title)

    def set_document_date(self, date):
        return self.browser.set_piece_date(date)

    def set_document_payement_mode(self, mode):
        return self.browser.set_piece_reglement_mode(mode)

    def set_document_payement_due(self, due):
        return self.browser.set_piece_reglement_echeance(due)
        
    def iter_expenses(self):
        return self.browser.iter_frais()

    def iter_expenses_lines(self, _id):
        return self.browser.iter_frais_lignes(_id)

    def create_expense(self, title):
        return self.browser.create_frais(title)

    def set_expense_person(self, individu):
        return self.browser.set_frais_individu(individu)

    def set_expense_activity(self, activity):
        return self.browser.set_frais_activite(activity)

    def set_expense_line_activity_charged(self, activity):
        return self.browser.set_frais_ligne_activite_a_imputer(activity)

    def set_expense_title(self, activity):
        return self.browser.set_frais_activite(activity)

    def submit_expense(self):
        return self.browser.submit_frais()

    def transmit_expense(self):
        return self.browser.transmet_frais()

    def valid_expense(self):
        return self.browser.valid_frais()

    def create_expense_line(self, typeId):
        return self.browser.create_frais_ligne(typeId)

    def edit_expence(self):
        return self.browser.iter_frais_ligne()

    def edit_expenses_line(self, _id):
        return self.browser.edit_frais_ligne(_id)
        
    def valid_expenses_line(self, _id):
        return self.browser.valide_frais_ligne()

    def init_create_client(self, _id):
        return self.browser.init_create_client(_id)

    def create_client(self, _id):
        return self.browser.create_client(_id)

    def save_client(self, force):
        return self.browser.save_client(force)

    def change_structure(self, _id):
        return self.browser.change_structure(_id)
