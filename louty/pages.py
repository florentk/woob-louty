# -*- coding: utf-8 -*-


from woob.browser.pages import HTMLPage, CsvPage, LoginPage, LoggedPage, JsonPage, pagination
from woob.browser.elements import  ItemElement, method, DictElement, ListElement
from woob.browser.filters.json import Dict
from woob.browser.filters.html import Attr, AbsoluteLink
from woob.browser.filters.standard import (
    CleanText, CleanDecimal, Field, Format, BrowserURL,
    Regexp, Date, AsyncLoad, Async, Eval, RegexpError, Env,
    Currency as CleanCurrency,
)

from woob.capabilities.commercial import Bill, Line
from woob.capabilities.customer import Customer
from woob.capabilities.expense import Line as ExpenseLine, Expense
from colorama import Fore, Back, Style

import requests

from time import localtime, strftime
from datetime import datetime
from lxml import etree
from io import StringIO
from unidecode import unidecode

__all__ = ['LoginPage','AccueilPage','ErrorPage'
           'ClientsPage','PreClientsPage',
           'DocsPage','PreDocsPage',
           'PiecePage','PieceEditPage',
           'NotesFraisPage','NoteFraisPage','NoteFraisEditPage']

#https://appli.louty.net/AchatsFrais/NoteFraisEdit/RechercheIndividu

#filter: delbar
#voirActivitesInactives: false
#voirTousIndividus: false
#date: 
#dateFin: 05/12/2020 23:20:43
#structureCouranteId: 42
#individuCourantId: 13556

#{"html":"\r\n\u003ctable id=\"liste_individu\" class=\"table table-condensed table-striped table-hover\"\u003e\r\n  \u003ctbody\u003e\r\n    \u003ctr\u003e\r\n      \u003cth class=\"listeIndividuNomPrenom\"\u003eNom Prénom\u003c/th\u003e\r\n      \u003cth class=\"listeIndividuCompte\"\u003eSuffixe compte\u003c/th\u003e\r\n      \u003cth class=\"listeIndividuActivites\"\u003eActivités\u003c/th\u003e\r\n    \u003c/tr\u003e\r\n    \r\n    \u003ctr\u003e\r\n      \u003ctd class=\"listeIndividuNomPrenom\"\u003e\u003ca href=\"#\" id=\"btnIndividuNom_20567\" individustructureid=\"18829\"\u003eDELBAR FOULQUES\u003c/a\u003e\u003c/td\u003e\r\n      \u003ctd class=\"listeIndividuCompte\"\u003e\u003ca href=\"#\" id=\"btnIndividuCompte_20567\" individustructureid=\"18829\" title=\"DELFO0\"\u003eDELFO0\u003c/a\u003e\u003c/td\u003e\r\n      \u003ctd class=\"listeIndividuActivites\"\u003e\u003ca href=\"#\" id=\"btnIndividuActivites_20567\" individustructureid=\"18829\" title=\"AADELFO0 - DELBAR FOULQUES\"\u003eAADELFO0\u003c/a\u003e\u003c/td\u003e\r\n    \u003c/tr\u003e\r\n    \r\n    \u003ctr\u003e\r\n      \u003ctd class=\"listeIndividuNomPrenom\"\u003e\u003ca href=\"#\" id=\"btnIndividuNom_13556\" individustructureid=\"12096\"\u003eKAISSER FLORENT\u003c/a\u003e\u003c/td\u003e\r\n      \u003ctd class=\"listeIndividuCompte\"\u003e\u003ca href=\"#\" id=\"btnIndividuCompte_13556\" individustructureid=\"12096\" title=\"KAIFL0\"\u003eKAIFL0\u003c/a\u003e\u003c/td\u003e\r\n      \u003ctd class=\"listeIndividuActivites\"\u003e\u003ca href=\"#\" id=\"btnIndividuActivites_13556\" individustructureid=\"12096\" title=\"AAKAIFL0 - KAISSER Florent\nAACCOMU0 - CCOMMUNEIDEE\nAALOUMA0 - LOUIS Marian\nAAPARPA0 - PARRA ORTEGA Pablo\nAADELFO0 - DELBAR FOULQUES\"\u003eAAKAIFL0, AACCOMU0, AALOUMA0, AAPARPA0, AADELFO0\u003c/a\u003e\u003c/td\u003e\r\n    \u003c/tr\u003e\r\n    \r\n\u003c/table\u003e\r\n","success":true}


#---------------------

#https://appli.louty.net/AchatsFrais/NoteFraisEdit/GetActiviteUniqueForIndividu

#individuId: 20567
#structureCouranteId: 42
#individuCourantId: 13556

#{"success":true,"activiteId":17552,"activiteCode":"AADELFO0","activiteLibelle":"DELBAR FOULQUES"}

class LoginPage(HTMLPage):
    def on_loaded(self):
        pass##

    def login(self, login, password):
        data = {}
        data['login'] = login
        data['password'] = password
        return self.browser.location(self.browser.absurl('api/authentication/login'), data=data)

class NoteFraisPage(LoggedPage, HTMLPage):
    
    
    def on_load(self):
        self.structureCouranteId = str(Attr('//input[@id="structureCouranteId"]', 'value', default=None)(self.doc))
        self.individuCourantId = str(Attr('//input[@id="individuCourantId"]', 'value', default=None)(self.doc))

        self.noteFraisId = str(Attr('//input[@id="noteFraisId"]', 'value', default=-1)(self.doc))
        self.numeroVersion = str(Attr('//input[@id="numeroVersion"]', 'value', default=0)(self.doc))
        self.versionHash = str(Attr('//input[@id="versionHash"]', 'value', default="")(self.doc))
        self.sessionId = str(Attr('//input[@id="sessionId"]', 'value', default=None)(self.doc))
        #self.duplicationFromId = str(Attr('//input[@id="duplicationFromId"]', 'value', default=None)(self.doc))
        #self.annulationId =  str(Attr('//input[@id="annulationId"]', 'value', default=False)(self.doc))

        self.individuStructureId = str(Attr('//input[@id="individuStructureId"]', 'value', default=self.structureCouranteId)(self.doc))
        self.individuId = str(Attr('//input[@id="individuId"]', 'value', default=self.individuCourantId)(self.doc))

        self.activiteId = str(Attr('//input[@id="activiteId"]', 'value', default=None)(self.doc))
        self.activiteCode = str(Attr('//input[@id="activiteCode"]', 'value', default=None)(self.doc))

        self.date = str(Attr('//input[@id="noteDate"]', 'value', default="")(self.doc))
        if(self.date==""): self.date = strftime("%d/%m/%Y", localtime())
        self.titre = str(Attr('//input[@id="titre"]', 'value', default="")(self.doc))
        if(self.titre == ""): self.titre = strftime("Frais Importés au %d%m%Y", localtime())

        #self.etat = str(Attr('//input[@id="etat"]', 'value', default="")(self.doc))

    @method
    class iter_lignes(ListElement):
        item_xpath = '//table[@id="table_noteFrais"]/tbody/tr[not(contains(@id,"nflId_-"))]'

        class item(ItemElement):
            klass = ExpenseLine

            obj_id = obj_purchase_id = Regexp(CleanText('./@id'),r'nflId_(\d+)')
            obj_position = CleanDecimal('./td/div/input[contains(@id,"nflOrdre_")]/@value')
            obj_purchase_type_id = CleanDecimal('./td/input[contains(@id,"nflTypAchatId_")]/@value')
            obj_date = Date(CleanText('./td/input[contains(@id,"nflDate_")]/@value'))
            obj_vat = CleanDecimal('./td/div/input[contains(@id,"nflMontantTVA_")]/@value', replace_dots=True)
            obj_amount = CleanDecimal('./td/div/input[contains(@id,"nflMontantHT_")]/@value', replace_dots=True)
            obj_activity_id = CleanDecimal('./td/div/div/input[contains(@id,"nflActiviteId_")]/@value')
            obj_description = CleanText('./td/div/input[contains(@id,"nflObjet_")]/@value')
            obj_supplier_id = CleanDecimal('./td/div/div/input[contains(@id,"nflFournisseurId_")]/@value')
            obj_supplier_description = CleanText('./td/div/div/input[contains(@id,"nflFournisseurLibelle_")]/@value')


class NotesFraisPage(LoggedPage, HTMLPage):

    @method
    class iter_documents(ListElement):
        item_xpath = '//table[@id="table_noteFrais"]/tbody/tr'

        class item(ItemElement):
            klass = Expense

            obj_id = obj_expense_id = Regexp(CleanText('./@id'),r'tr_nfr_(\d+)') 
            obj_label = CleanText('./td[contains(@class, "noteFrais_Objet")]/@title')
            obj_date = Date(CleanText('./td/a/span[contains(@id,"noteFraisDate_")]'))
            obj_id_chrono = CleanText('./td[not(contains(@class,"noteFrais_Numero"))]/a[contains(@href,"noteFraisId")]')
            obj_third_party = CleanText('./td[contains(@class, "noteFrais_Tiers")]/@title')
            obj_amount_vat = CleanDecimal('./td[contains(@class, "numeric")]', replace_dots=True)
            obj_state = CleanText('./td/div[contains(@class,"pastille")]/@title')


class NoteFraisEditPage(NoteFraisPage):

    def recherche_activite_id(self, activite):
        r = self.browser.open( self.absurl('../ActiviteCommons/GetActivitesForModalListeActivites'), 
          data = {
            'recherche': activite,
            'inactive': 'false',
            'codeFonctionnalite': 'NF_NOTEF_CREATION',
            'voirColonneIcone': 'false',
            'structureCouranteId':self.structureCouranteId,
            'individuCourantId':self.individuCourantId
        }
        ).json()

        tree = etree.parse(StringIO(r['html']), etree.HTMLParser())

        actId = tree.xpath('//table/tbody/tr/td/a')[0].xpath('@actid')[0]

        return actId


    def get_activite_unique(self, individuId):
        data = {
            'individuId': individuId,
            'structureCouranteId':self.structureCouranteId,
            'individuCourantId':self.individuCourantId,
            'sessionId':self.sessionId,
            'pieceId':-1
        }
        r = self.browser.open( self.absurl('GetActiviteUniqueForIndividu'), 
          data = data
        ).json()
        return r['activiteId']

    def recherche_individu_id(self, individu):
        r = self.browser.open( self.absurl('RechercheIndividu'), 
          data = {
            'filter': individu,
            'voirActivitesInactives': 'false',
            'voirTousIndividus': 'false',
            'date': '',
            'dateFin': datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 
            'structureCouranteId':self.structureCouranteId,
            'individuCourantId':self.individuCourantId
        }
        ).json()
        tree = etree.parse(StringIO(r['html']), etree.HTMLParser())
        try :
            individus = tree.xpath('//table/tbody/tr/td[@class="listeIndividuNomPrenom"]/a')
            individu_found = [(e.xpath('@id')[0].split('_')[1], e.text) for e in individus if e.text.upper() == individu.upper()]
            return individu_found[0][0]
        except Exception:
            raise Exception("Unable to find " + individu)

    def recherche_type_achat(self, libelle):
        data = {
          'recherche': libelle,
          'date': "",
          'voirTous': 'false',
          'activiteId': self.activiteId,
          'structureCouranteId':self.structureCouranteId,
          'individuCourantId':self.individuCourantId
        }

        r = self.browser.open( self.absurl('../TypAchat/RechercheTypAchats'), 
          data = data
        ).json()
        tree = etree.parse(StringIO(r['html']), etree.HTMLParser())

        el = tree.xpath('//tbody/tr/td/a')[0]

        return int(el.xpath('@id')[0].split('_')[1])

    def recherche_founisseur(self, description):
        data = {
          'libelle': description,
          'tiersId': self.individuCourantId,
          'structureCouranteId':self.structureCouranteId,
          'individuCourantId':self.individuCourantId
        }
        
        r = self.browser.open( self.absurl('../GetFournisseurs'), 
          data = data
        ).json()
        tree = etree.parse(StringIO(r['html']), etree.HTMLParser())
        #print(etree.tostring(tree.getroot(),pretty_print=True, method="html"))
        el = tree.xpath('//table/tr/td/a')[0]

        return (int(el.xpath('@id')[0].split('_')[1]),el.xpath('@nom')[0])
        
#TODO list fournisseur
        
    def submit(self, action="Save"):
        data = {}
        data['id'] = self.noteFraisId
        data['sessionId'] = self.sessionId
        data['numeroVersion'] = self.numeroVersion
        data['duplicationFromId'] = ""
        data['annulationId'] = ""
        data['versionHash'] = self.versionHash
        data['memoInterne'] = ""
        data['memoTete'] = ""
        data['activiteId'] = self.activiteId
        data['individuId'] = self.individuId
        data['date'] = self.date
        data['objet'] =  self.titre
        #data['etatLibelle'] =  self.etat
        data['structureCouranteId'] = self.structureCouranteId
        data['individuCourantId'] = self.individuCourantId

        ret =  self.browser.open( self.absurl("Save"), data = data).json()
        if(ret["success"]):
            self.noteFraisId = ret["noteFraisId"]
            print(Back.GREEN)
            print("Note de frais ajouté")
            print(Style.RESET_ALL)
        else:
            print(Back.RED)
            print("ERROR : " + "Error to submit expense : " + ", ".join(ret["errors"]))
            print(Style.RESET_ALL)
        
        return ret

    def get_base_data_operation(self):
        data = {}
        data['noteFraisId'] = self.noteFraisId
        data['structureCouranteId'] = self.structureCouranteId
        data['individuCourantId'] = self.individuCourantId
        return data

    def transmet_imprime(self):
        data = self.get_base_data_operation()

        print("transmet")
        r = self.browser.open( self.absurl("../NoteFrais/Transmet"), data = data).json()

        print("imprime")
        r = self.browser.open( self.absurl("../NoteFrais/RouteImpression"), data = data).json()

        return r

    def valid(self):
        data = self.get_base_data_operation()
        print("valide")
        r = self.browser.open( self.absurl("../NoteFrais/Valide"), data = data).json()
        return r

    def valide_ligne(self, ligne):
        data = self.get_base_data_operation()

        data['id'] = ligne.purchase_id
        data['ordre'] = ligne.position
        data['objet'] = ligne.description[:40] #Louty limite l'objet à 40 caractères
        data['date'] = ligne.date.strftime("%d/%m/%Y")
        data['montant'] = ("%.2f" % ligne.amount).replace('.', ',')
        data['htTtc'] = "HT"
        data['montantTVA'] = ("%.2f" % ligne.vat).replace('.', ',')
        data['typAchatId'] = self.recherche_type_achat(ligne.purchase_type)
        data['typAchatLibelle'] = ""
        data['typAchatMemo'] = ""

        data['fournisseurLibelle'] = ligne.supplier_description[:30] #Louty limite le nom du fournisseur à 30 caractères
        data['fournisseurId'] = ligne.supplier_id if (ligne.supplier_id != -1) else ""

        data['activiteId'] = ligne.activity_id
        data['activiteCode'] = ""
        data['activiteLibelle'] = ""

        data['individuId'] = self.individuId
        data['dateNoteFrais'] = self.date
        data['sessionId'] = self.sessionId

        ret = self.browser.open( self.absurl('ValidateRow'), data = data).json()

        if(ret["success"]):
            print("Ajout d'une ligne", end ="")
            print(Fore.GREEN)
            print("OK")
            print(Style.RESET_ALL)
        else:
            print(Back.RED)
            print("ERROR : " + "Error to add the line : " + ", ".join(ret["errors"]))
            print(Style.RESET_ALL)

        return ret

    def create_ligne(self, _id):
        ligne = ExpenseLine()

        r = self.browser.open( self.absurl('AddLigne'), 
          data = {'nflId': _id,
          
                  'noteFraisId': self.noteFraisId,
                  'showDetailCompte': False,
                  
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}
        ).json()

        tree = etree.parse(StringIO(r['html']), etree.HTMLParser())
        returned_id = int(tree.xpath('//tr/@id')[0].split('_')[1])
        ligne.purchase_id = _id
        ligne.position = int(tree.xpath('//tr/td/div[@id="nflDivOrdre_%d"]/input/@value' % returned_id)[0])
        ligne.activity_id = int(self.activiteId)
        ligne.supplier_description = u"N/A"
        ligne.description = u"N/A"
        ligne.date =  datetime.strptime(self.date, "%d/%m/%Y")
        ligne.supplier_id = -1
        ligne.purchase_type = u""

        return ligne

class PiecePage(LoggedPage, HTMLPage):

    def on_load(self):
        #TODO mettre dans class parente
        self.date = str(Attr('//input[@id="date"]', 'value', default="")(self.doc))
        if(self.date==""): self.date = strftime("%d/%m/%Y", localtime())
        self.titre = str(Attr('//input[@id="titre"]', 'value', default="")(self.doc))
        if(self.titre == ""): self.titre = strftime("FC_%d%m%Y_", localtime())
        self.modeReglementId = Attr('//select[@id="modeReglementId"]/option[@selected]', 'value', default=None)(self.doc)
        self.echeanceReglementId = Attr('//select[@id="echeanceReglementId"]/option[@selected]', 'value', default=1)(self.doc)
        self.structureCouranteId = str(Attr('//input[@id="structureCouranteId"]', 'value', default=None)(self.doc))
        self.individuCourantId = str(Attr('//input[@id="individuCourantId"]', 'value', default=None)(self.doc))
        self.pieceId = str(Attr('//input[@id="pieceId"]', 'value', default=-1)(self.doc))
        self.sessionId = str(Attr('//input[@id="sessionId"]', 'value', default=None)(self.doc))
        self.clientId = str(Attr('//input[@id="clientId"]', 'value', default=None)(self.doc))
        self.activiteId = str(Attr('//input[@id="activiteId"]', 'value', default=None)(self.doc))
        self.activitePresentationId = Attr('//select[@id="dropDownIdentiteVisuelle"]/option[@selected]', 'value', default=None)(self.doc)
        self.typAdresseId = Attr('//select[@id="dropDownAdresseImpression"]/option[@selected]', 'value', default=1)(self.doc)

        self.modeReglements = {}
        for option in self.doc.xpath('//select[@id="modeReglementList"]/option'):
            if(option.text):
                self.modeReglements[unidecode(option.text).lower()] = option.values()[0]

        self.echeanceReglement = {}
        for option in self.doc.xpath('//select[@id="echeanceReglementList"]/option'):
            if(option.text):
                self.echeanceReglement[unidecode(option.text).lower()] = option.values()[0]

    def get_units(self):
        ret = self.browser.open( self.absurl('../../Unites/GetListeLRU'), method='POST')
        html = ret.json()['html']

        tree = etree.parse(StringIO(html), etree.HTMLParser())

        el = tree.xpath('//table/tr/td/a')
        self.units = {}
        for e in el:
            self.units[e.xpath('@unitesymbole')[0]] = e.xpath('@uniteid')[0]

    def get_articles(self):
        return self.browser.open( self.absurl('../../Search/article'), 
                  data = {'structureIds[0]': self.structureCouranteId,
                          'activiteIds[0]': self.activiteId,
                          'isDisponible': 'true',
                          'isAcompte': 'false',
                          'isAvance': 'false',
                          'codeAsChampTextDuResultat': 'true',
                          'page': 1,
                          'structureCouranteId':self.structureCouranteId,
                          'individuCourantId':self.individuCourantId}
        ).json()

    def recherche_client(self, accountId):
        data = {
          'filter': accountId,
          'activiteId': self.activiteId,
          'voirTous': 'false',
          'skip': 0,
          'take': 1,
          'pieceId': self.pieceId,
          'structureCouranteId':self.structureCouranteId,
          'individuCourantId':self.individuCourantId
        }
        r = self.browser.open( self.absurl('../../Clients/RechercheClients'), 
          data = data
        ).json()
        
        try:
            tree = etree.parse(StringIO(r['html']), etree.HTMLParser())
            #print(etree.tostring(tree.getroot(),pretty_print=True, method="html"))
            el = tree.xpath('//table/tr/td/a')[0]
            return (el.xpath('@orsid')[0],el.xpath('@libelle')[0])
        except Exception:
            raise Exception("Client %s non trouvé " % accountId)

    @method
    class iter_lignes(ListElement):
        item_xpath = '//table[@id="table_pli"]/tbody/tr[not(contains(@class,"tr_memo"))]'

        class item(ItemElement):
          klass = Line
          
          obj_id = Regexp(CleanText('./@id'),r'pli_(\d+)')
          obj_article_id = CleanDecimal('./td/input[contains(@id,"pli_article_key_")]/@value')
          obj_description = CleanText('./td/input[contains(@id,"pli_libelle_")]/@value')
          obj_vat_rate = CleanDecimal('./td/input[contains(@id,"pli_tauxTva_")]/@value', replace_dots=True)
          obj_price_unit = CleanDecimal('./td/input[contains(@id,"pli_prixunit_")]/@value', replace_dots=True)
          obj_quantity = CleanDecimal('./td/input[contains(@id,"pli_quantite_")]/@value', replace_dots=True)
          obj_unit_id = CleanText('./td/input[contains(@id,"pli_unite_key_")]/@value')
          obj_amount = CleanDecimal('./td/input[contains(@id,"pli_ht_")]/@value', replace_dots=True)
          obj_amount_vat = CleanDecimal('./td/input[contains(@id,"pli_ttc_")]/@value', replace_dots=True)
          obj_activity_id = CleanText('./td/input[contains(@id,"pli_activite_key_")]/@value')
          obj_memo = CleanText('./following-sibling::tr[1]/td/textarea')
          
          def obj_vat(self):
              return "TTC" in CleanText('./td/a/div/input[contains(@id,"mode_ht_ttc_")]')(self)
              

class PieceEditPage(PiecePage):
    def set_client(self, accountId):
        (self.clientId,label) = self.recherche_client(accountId)
        self.set_titre("FC_%s_%s" % (strftime("%d%m%Y", localtime()), label))

    def set_titre(self, titre):
        self.titre = titre

    def set_date(self, date):
        self.date = date

    def set_reglement_mode(self,mode):
        mode_normal = unidecode(mode).lower()
        if(mode_normal in  self.modeReglements):
            self.modeReglementId = self.modeReglements[mode_normal]
        else:
            self.modeReglementId = list(self.modeReglements.values())[0]

    def set_reglement_echeance(self,echeance):
        echeance_normal = unidecode(echeance).lower()
        if(echeance_normal in  self.echeanceReglement):
            self.echeanceReglementId = self.echeanceReglement[echeance_normal]
        else:
            self.echeanceReglementId = list(self.echeanceReglement.values())[0]

    def printLigne(self, ligne):
        print("-----------------------------")
        print(str(ligne.id))
        print(str(ligne.article_id))
        print(ligne.description)
        print(str(ligne.price_unit))
        print(str(ligne.unit_id))
        print(str(ligne.quantity))
        print(str(ligne.amount))
        print(str(ligne.amount_vat))
        print(str(ligne.activity_id))
        print(ligne.memo)
        print()

    def valide_ligne(self, ligne):

        ligne = self.switchHtTtc(ligne)
        ligne = self.update_prix_unit(ligne)
        ligne = self.update_quantite(ligne)
        ligne = self.update_montantTTC(ligne)

        data = {}
        data['id'] = str(ligne.id)
        data['articleId'] = str(ligne.article_id)
        data['libelle'] = ligne.description
        data['prixUnit'] = str(ligne.price_unit).replace('.', ',')
        data['uniteId'] = self.units[str(ligne.unit_id)]
        data['quantite'] = str(ligne.quantity).replace('.', ',')
        data['montantHT'] = str(ligne.amount).replace('.', ',')
        data['montantTTC'] = str(ligne.amount_vat).replace('.', ',')
        data['activiteId'] = str(ligne.activity_id)
        data['memo'] = ligne.memo
        data['pieceId'] = self.pieceId
        data['sessionId'] = self.sessionId
        data['structureCouranteId'] = self.structureCouranteId
        data['individuCourantId'] = self.individuCourantId

        r= self.browser.open( self.absurl('../ValideLigne'), data = data).json()

        return r
    
    def update_montants(self, montants, ligne):
        #ligne.price_unit = float(montants['prixUnit'].replace(',', '.'))
        #ligne.quantity = float(montants['quantite'].replace(',', '.'))
        ligne.vat_rate = float(montants['tauxTva'].replace(',', '.'))
        ligne.amount = float(montants['montantHT'].replace(',', '.'))
        ligne.amount_vat = float(montants['montantTTC'].replace(',', '.'))
        ligne.vat = montants['modePrixUnit'] == 'TTC'
        return ligne

## switchHtTtc
## ../SwitchHtTtc  
## pliId=-1&mode=HT&pieceId=-3&sessionId=4&structureCouranteId=501&individuCourantId=27946
##  line.vat

    def switchHtTtc(self, ligne):
        data = {'pliId': ligne.id,
                  'mode': 'TTC' if ligne.vat else 'HT',
                  'pieceId': self.pieceId,
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}

        r = self.browser.open( self.absurl('../SwitchHtTtc'), 
          data = data
        ).json()

        return self.update_montants(r['infoMontantsLigne'], ligne)

    def update_montantTTC(self, ligne):
        data = {'pliId': ligne.id,
                  'montantTTC': str(ligne.amount_vat).replace('.', ','),
                  'pieceId': self.pieceId,
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}

        r = self.browser.open( self.absurl('../UpdateMontantTTC'), 
          data = data
        ).json()

        return self.update_montants(r['infoMontantsLigne'], ligne)

    def update_quantite(self, ligne):
        data = {'pliId': ligne.id,
                  'quantite': str(ligne.quantity).replace('.', ','),
                  'pieceId': self.pieceId,
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}

        r = self.browser.open( self.absurl('../UpdateQuantite'), 
          data = data
        ).json()

        return self.update_montants(r['infoMontantsLigne'], ligne)

    def update_prix_unit(self, ligne):
        data = {'pliId': ligne.id,
                  'prixUnit': str(ligne.price_unit).replace('.', ','),
                  'pieceId': self.pieceId,
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}
        r = self.browser.open( self.absurl('../UpdatePrixUnit'), 
          data = data
        ).json()
        return self.update_montants(r['infoMontantsLigne'], ligne)

    def calcule_date_echeance(self):
        data = {'datePiece': self.date,
                  'echeanceReglementId': self.echeanceReglementId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}
        r = self.browser.open( self.absurl('../../EcheanceReglement/CalculeDateEcheance'), 
          data = data
        ).json()
        
        return r['dateEcheance']

    def create_ligne(self, articleCode):
        ligne = Line()

        r = self.get_articles()
        articles = r['items']
        matched_article = filter(lambda a: a['text'] == articleCode, articles)
        l_matched_article = list(matched_article)
        if(len(l_matched_article) > 0):
          article = l_matched_article[0]
          ligne.article_id = article['id']
          ligne.description = article['articleStandardText']
        else:
          raise Exception("Code article %s n'existe pas" % articleCode)

        self.get_units()
	
        r = self.browser.open( self.absurl('../GetNewRow'), 
          data = {'articleCode':articleCode,
                  'activiteId': self.activiteId,
                  'pieceId': self.pieceId,
                  'sessionId':self.sessionId,
                  'structureCouranteId':self.structureCouranteId,
                  'individuCourantId':self.individuCourantId}
        ).json()

        ligne.id = r['id']
        
        ligne.activity_id = self.activiteId
        ligne.unit_id = 16
        ligne.memo = u""

        return ligne

    def submit(self, action):
        form = self.get_form()
        #form['pieceTypeId'] = form['typeParse']
        form['pieceTypeId'] = "FACTURE_CLIENT"
        form['memoInterne'] = ""
        form['id'] = self.pieceId
        form['structureCouranteId'] = self.structureCouranteId
        form['individuCourantId'] = self.individuCourantId
        form['sessionId'] = self.sessionId
        form['clientId'] = self.clientId
        form['activitePresentationId'] = self.activitePresentationId
        form['typAdresseId'] = self.typAdresseId
        form['date'] = self.date
        form['titre'] =  self.titre
        form['clientId'] =  self.clientId
        form['modeReglementId'] =  self.modeReglementId
        form['echeanceReglementId'] =  self.echeanceReglementId
        form['dateEcheance'] =  self.calcule_date_echeance()
        ret = self.browser.open( self.absurl('../%s' % action), data = form).json()

        if(action == "Enregistre"):
            if(ret["success"]):
                self.pieceId = ret["pieceId"]
            else:
                raise Exception("Error to submit document : " + str(ret))

        return ret

    def transmit(self):

        data = {}
        data['pieceId'] = str(self.pieceId)
        data['structureCouranteId'] = self.structureCouranteId
        data['individuCourantId'] = self.individuCourantId

        #print("transmet")
        r = self.browser.open( self.absurl('../../Pieces/TransmetPiece'), data = data).json()

        #print("imprime")
        self.browser.open( self.absurl("../../Pieces/RouteImpressionPiece"), data = data).json()

        return r



    def valide(self):

        data = {}
        data['pieceId'] = str(self.pieceId)
        data['structureCouranteId'] = self.structureCouranteId
        data['individuCourantId'] = self.individuCourantId

        #print("valide")
        r = self.browser.open( self.absurl('../../Pieces/ValidePiece'), data = data).json()

        #print("imprime")
        self.browser.open( self.absurl("../../Pieces/RouteImpressionPiece"), data = data).json()

        return r

class ErrorPage(LoggedPage, HTMLPage):
    def on_load(self):
        print(self.doc)

#class DocsPage(LoggedPage, CsvPage):
class DocsPage(LoggedPage, HTMLPage):
    #@pagination
    @method
    class iter_documents(ListElement):
        #next_page = AbsoluteLink('//div[@class="pagination"]/ul/li[last()][not(@class)]/a', default=None)
        item_xpath = '//table[@id="table_pie"]/tbody/tr'

        class item(ItemElement):
            klass = Bill
            
            obj_id = Regexp(CleanText('./@id'),r'pie_(\d+)') 
            obj_label = CleanText('./td[contains(@class, "col_titre")]/@title')
            obj_id_chrono = CleanText('./td[not(contains(@class,"col_titre"))]/a[contains(@href,"PiecesEdit")]')
            obj_type = CleanText('./td/span[contains(@id,"typePiece_")]')
            obj_date = Date(CleanText('./td/span[contains(@id,"datePiece_")]'))
            obj_customer_num = CleanText('./td/span[contains(@id,"numClientPiece_")]')
            obj_customer = CleanText('./td/span[contains(@id,"nomClient_")]')
            obj_amount = CleanDecimal('./td/span[contains(@id,"montantHTPiece_")]', replace_dots=True)
            obj_amount_vat = CleanDecimal('./td/span[contains(@id,"montantTTCPiece_")]', replace_dots=True)
            obj_currency = u'€'
            obj_state = CleanText('./td/div[contains(@class,"pastille")]/@title')
            obj_amount_to_paid = CleanDecimal('./td/span[contains(@id,"netAPayer_")]', replace_dots=True)

            def obj_duedate(self):
              if(CleanText('./td/span[contains(@id,"dateEcheance_")]')(self)):
                return Date(CleanText('./td/span[contains(@id,"dateEcheance_")]'))(self)
              else:
                return None
                
            def obj_pending(self):
              return "EN_ATTENTE" in CleanText('./td/div[contains(@id,"etatReglement_")]/@etat')(self)
            
            #montantTTCPiece
#    HEADER = 1
#    FMTPARAMS = {'delimiter': str(';')}
#    @method
#    class iter_documents(DictElement):
#        
#        class item(ItemElement):
#            klass = Bill    
#            #obj_label = Dict('\xef\xbb\xbfTitre')
#            obj_date = Date(Dict(u'Date'))
#            obj_type = Dict(u'Type')
#            obj_amount = CleanDecimal(Dict(u'Montant T.T.C.'), replace_dots=True)
#            obj_id_chrono = Dict(u'Numéro chrono')
#            obj_amount_to_paid = CleanDecimal(Dict(u'Net à payer'), replace_dots=True)
#            obj_currency = u'€'
#            obj_state = Dict(u'Etat') 
#            obj_customer = Dict(u'Client')
#            
#            def obj_duedate(self):
#              if(Dict(u'Date échéance')(self)):
#                return Date(Dict(u'Date échéance'))(self)
#              else:
#                return None
#            
#            def obj_pending(self):
#              return "EN_ATTENTE" in Dict(u'Règlement')(self)
#                      
#    def decode_row(self, row, encoding):
#      return CsvPage.decode_row(self, row, encoding)
 
class ClientsPage(LoggedPage, CsvPage):
    HEADER = 1
    FMTPARAMS = {'delimiter': str(';')}
    
    @method
    class iter_clients(DictElement):
        class item(ItemElement):
            klass = Customer

            #TODO titre not found ???
            #obj_title = Dict(u'Titre')
            obj_name = Dict(u'Nom')
            obj_firstname = Dict(u'Prénom')
            obj_contactname =  Dict(u'Contacts')
            obj_id = obj_account_id =  Dict(u'Numéro compte')
            obj_account_name = Dict(u'Libellé compte')
            obj_iban = Dict(u'Iban')
            obj_payment_mode = Dict(u'Mode de règlement')
            obj_payment_term = Dict(u'Echéance de règlement')
            obj_postal_code = Dict(u'Code postal')
            obj_city = Dict(u'Commune')
            obj_country = Dict(u'Pays')
            #obj_service = Dict(u'Destinataire/Service')
            #obj_building = Dict(u'Entrée/Bâtiment')
            obj_street_number = Dict(u'Numéro')
            obj_street_name = Dict(u'Voie')
            obj_street_extra = Dict(u'Mention spéciale')
            obj_email = Dict(u'Courriel')
            obj_telephone = Dict(u'Téléphone')

    def decode_row(self, row, encoding):
      return CsvPage.decode_row(self, row, encoding)

class PreClientsPage(JsonPage, LoggedPage):
    INIT = 1
    
class PreDocsPage(JsonPage, LoggedPage):
    INIT = 1

class UpdatePage(JsonPage, LoggedPage):
    INIT = 1

class AccueilPage(HTMLPage, LoggedPage):
    def on_load(self):
        self.structureCouranteId = Attr('//input[@id="structureCouranteId"]', 'value', default=None)(self.doc)
        self.individuCourantId = Attr('//input[@id="individuCourantId"]', 'value', default=None)(self.doc)

